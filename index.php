<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/mediaquery.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/intlTelInput.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
<div id="nahual-popUp">
<div id="popUp-mastercontainer">
  <div id="title-popUp-container">
    <img id="popup-header" src="" alt="">
  </div>
<div id="maincontnahual">
<div id="nahual-container">
<!-- GAME NOTIFICATION -->
    <div id="game-notification">
      <div class="title-nitification">
        <h2 id="not_title"></h2>
      </div>
      <div class="text-notification">
        <p id="not_txt"></p>
      </div>
      <div class="input-notification">
        <label for="name" class="label-name" id="not_input_placeholder_name"></label>
        <input type="name" class="email-input" name="nom" value="" id="not_input_name">

        <label for="email" class="label-email" id="not_input_placeholder_email"></label>
        <input type="email" class="email-input" name="email" value="" id="not_input_field">
      </div>
      <div class="button-notification">
        <button type="submit" name="button" id="not_btn"></button>
      </div>
    </div>
<!-- GAME NOTIFICATION END -->
  <div id="blackoverlay"></div>
  <div id="formoverlay">
    <div id="borderedcont">
      <h3 id="res-nahual-name"></h3>
      <img id="res-nahual-img" src=""></img>
      <form id="nahual-form" class="fcontainer" name="FE_Form" action="#" onsubmit="return false;" method="post">
        <p id="t1"></p>
        <p id="t2"></p>
        <p id="t3"></p>
        <div id = "form-bot-container"><div id = "contmail"><p class="arrow_box" id="arrowmail"></p></div>
        <input id="fieldemail" name="email" type="text"  value=""><br></div>
      </form>
      <form id="nahual-complete-form" name="FEC_Form" action="#" onsubmit="return false;" method="post">
        <div id="text_help">
          <p id="t12"></p>
          <p id="t22"></p>
        </div>
        <div id="separador">
          <span class="separadores"></span>
          <span class="separadores"></span>
          <span class="separadores"></span>
        </div>
        <div id="text_raspar">
          <h2 id="raspar_info"></h2>
        </div>
        <div id="gender_container">
          <p id="gender_txt1"></p>
          <p id="gender_txt2"></p>
          <img id="genderfemale" class="genderopts"></img>
          <img id="gendermale" class="genderopts"></img>
        </div>
        <div id="raspadito">
          <div id="scratch_here">
          </div>
        </div>
        <div id="allinputs">
        <div id="content-input">
          <div class="input-text">
            <p id="raspar_input_info"></p>
          </div>
          <div class="container-form-element">
            <input id="field-name" name="prenom" type="text"  class="formfields" value="">
            <button id="btnrasparsend" type="submit" name="button"></button>
          </div>
      </div>
          <div class="tel-titulo" id="ribbon-raspar">
        </div>
      </div>
      </form>
      <div id="form-result-message"></div>
    </div>
  </div>

<iframe name="hiddenFrame" width="0" height="0" border="0" style="display: none;"></iframe>

  <form id="regform" method="post" action="" target="hiddenFrame">
    <input type="hidden" id="fr_prenom" name="prenom">
    <input type="hidden" id="fr_v_id" name="v_id">
    <input type="hidden" id="fr_l_id" name="l_id">
    <input type="hidden" id="fr_bday" name="bday">
    <input type="hidden" id="fr_email" name="email">
    <input type="hidden" id="fr_sexo" name="sexo">
    <input type="hidden" id="fr_phone" name="phone">
    <input type="hidden" id="ajax" name="ajax" value="ingreso-form">
  </form>

  <img src="https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/rueda1.png" id="swheel"/>
  <img src="https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/centro-top.png" id="center-top"/>

  <div id="zodiac">
    <img id="zdimg">
    <h1 id="ztext"></h1>
    </img>
  </div>

  <input type="text" id="datpic" disabled="" value=""></input>

  <div id="inputoverlay" class="glow"></div>
  <div id="calendar_container"></div>

  <img src="https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/rueda2.png" id="bwheel"/>
  <img src="https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/centro-bot.png" id="center-bottom"/>
  <img src="https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/start_btn.png" id="start-btn"/>
</div>

  <img id="game-border" src="//cdn.dynamicyield.com/api/8766488/images/9dfc436daab4__marco.png"/>
</div>
</div>

</div>



<script type="text/javascript" src="js/nahual.js"></script>
</body>
</html>